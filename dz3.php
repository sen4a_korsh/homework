<?php

// №1*
echo '<h3>#1*</h3>';

$a = 42;
$b = 55;

if ($a < $b) {
    echo "$a меньше, чем $b";
} elseif ($a > $b) {
    echo "$a больше, чем $b";
} else {
    echo "$a и $b равны";
}

// №2*
echo '<h3>#2*</h3>';

$a = rand(5, 15);
$b = rand(5, 15);

if ($a < $b) {
    echo "$a меньше, чем $b";
} elseif ($a > $b) {
    echo "$a больше, чем $b";
} else {
    echo "$a и $b равны";
}

// №3*
echo '<h3>#3*</h3>';

$f = 'Korshenko';
$i = 'Semen';
$o = 'Alexeyevich';

echo $f . ' ' . $i[0] . '.' . $o[0] . '.';

// №4*
echo '<h3>#4*</h3>';

$a = rand(1, 99999);
$b = rand(0, 9);

echo "Цифра $b встречается " . substr_count($a, $b) . " раз(а) в числе $a ";

// №5*
echo '<h3>#5*</h3>';

$a = 3;
echo $a;
echo '<br>';

$a = 10;
$b = 2;
echo "<br>Сумма чисел $a и $b: ";
echo $a + $b;
echo "<br>Разность чисел $a и $b: ";
echo $a - $b;
echo "<br>Произвидение чисел $a и $b: ";
echo $a * $b;
echo "<br>Частное чисел $a и $b: ";
echo $a / $b;
echo '<br><br>';

$c = 15;
$d = 2;
$result = $c + $d;
echo $result . '<br><br>';

$a = 10;
$b = 2;
$c = 5;
echo $a + $b + $c . '<br><br>';

$a = 17;
$b = 10;
$c = $a - $b;
$d = rand(5, 15);

// №6*
echo '<h3>#6*</h3>';
$result = $c + $d;
echo $result;

// №7*
echo '<h3>#7*</h3>';
$text = 'Привет, Мир!';
echo $text . '<br><br>';

$text1 = 'Привет ';
$text2 = 'Мир!';
echo $text1 . $text2 . '<br><br>';

echo 60 * 60 . ' секунд в часе <br>';
echo 60 * 60 * 24 . ' секунд в сутках <br>';
echo 60 * 60 * 24 * 7 . ' секунд в неделе <br>';
echo 60 * 60 * 24 * 30 . ' секунд в месяце(30 дней) <br>';
echo '<br><br>';

// №8*
echo '<h3>#8*</h3>';

$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
$var %= 1;
echo $var . '<br><br>';

echo '<hr>';

// №1**
echo '<h3>#1**</h3>';
$hour = 18;
$minute = 24;
$second = 36;
echo $hour . ':' . $minute . ':' . $second . '<br><br>';

// №2**
echo '<h3>#2**</h3>';
$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text . '<br><br>';

// №3**
echo '<h3>#3**</h3>';

$bar = 10;
$foo = 'bar';
echo $$foo . '<br><br>';

// №4**
echo '<h3>#4**</h3>';

$a = 2;
$b = 4;
echo $a++ + $b; //6
echo $a + ++$b; //8
echo ++$a + $b++ . '<br><br>'; //9

// №5**
echo '<h3>#5**</h3>';
$a = 'boom4';
echo isset($a) ? 'Переменная $а существует<br>' : 'Переменной $а не существует<br>';
echo 'Переменная $а типа: ' . gettype($a) . '<br>';
echo is_null($a) ? 'Переменнная $а равна NULL<br>' : 'Переменная $а не равна NULL<br>';
echo empty($a) ? 'Перемнная $а пустая<br>' : 'Переменная $а не пустая<br>';
echo is_integer($a) ? 'Переменная $а чило<br>' : 'Переменная $а не число<br>';
echo is_double($a) ? 'Переменнная $а с плавающей точкой<br>' : 'Переменнная $а не с плавающей точкой<br>';
echo is_string($a) ? 'Переменная $а строка<br>' : 'Переменная $а не строка<br>';
echo is_numeric($a) ? 'В переменной $а есть число<br>' : 'В переменной $а нет числа<br>';
echo is_bool($a) ? 'Переменная $а типа bool<br>' : 'Переменная $а типа не bool<br>';
echo is_scalar($a) ? 'Переменнная $а скалярная<br>' : 'Переменнная $а не скалярная<br>';
echo is_array($a) ? 'Переменная $а является массивом<br>' : 'Переменная $а не является массивом<br>';
echo is_object($a) ? 'Переменная $а является объектом<br><br>' : 'Переменная $а не является объектом<br><br>';

echo '<hr>';

// №1***
echo '<h3>#1***</h3>';

$a = rand(0, 100);
$b = rand(0, 100);
$sum = $a + $b;
$proiz = $a * $b;

echo '$a=' . $a . '<br>';
echo '$b=' . $b . '<br>';
echo "Сумма чисел: $sum" . "<br>Произведение чисел: $proiz<br><br>";

// №2***
echo '<h3>#2***</h3>';

$a = rand(0, 10);
$b = rand(0, 10);
$sumKv = $a ** 2 + $b ** 2;
echo '$a=' . $a . '<br>';
echo '$b=' . $b . '<br>';
echo "Сумма чисел: $sumKv";

// №3***
echo '<h3>#3***</h3>';

$arr = [rand(0, 10), rand(0, 10), rand(0, 10)];
print_r($arr);
echo '<br> Сумма массива: ' . array_sum($arr) . '<br>';
$result = count($arr);
echo 'Среднее значение массива:' . array_sum($arr) / $result . '<br><br>';

// №4***
echo '<h3>#4***</h3>';

$x = rand(5, 10);
$y = rand(5, 10);
$z = rand(5, 10);
$W = ($x + 1) - 2 * ($z - 2 * $x + $y);

echo 'W=(x+1)−2(z−2x+y)<br>
      x=' . $x . '<br>
      y=' . $y . '<br>
      z=' . $z . '<br>
      W=' . $W . '<br><br>';

// №5***
echo '<h3>#5***</h3>';

echo '$a=' . $a . '<br>';
echo '$a % 3=' . $a % 3 . '<br>';
echo '$a % 5=' . $a % 5 . '<br>';
echo '$a + 120%=' . 2.2 * $a . '<br>';
echo '$a + 30%=' . 1.3 * $a . '<br>';

// №6***
echo '<h3>#6***</h3>';

$a = rand(100, 999);
$b = rand(100, 999);
echo '$a=' . $a . '<br>
      $b=' . $b . '<br>';
echo ($a * 0.40) + ($b * 0.84);
$a = (string)$a;
echo '<br>' . ($a[0] + $a[1] + $a[2]) . '<br><br>';

// №7***
echo '<h3>#7***</h3>';

$a = rand(100, 999);
$a = (string)$a;
$a[1] = '0';
echo $a[2] . $a[1] . $a[0];

// №8***
echo '<h3>#7***</h3>';

$a = rand(0, 10);
$b = rand(0, 10);
echo "Число $a ";
echo $a % 2 == 0 ? 'четное<br>' : 'не четное<br>';
echo "Число $b ";
echo $b % 2 == 0 ? 'четное<br>' : 'не четное<br>';
