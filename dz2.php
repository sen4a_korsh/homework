<?php
$myName = 'Семен';
$age = 20;
$pi = pi();

$arr = ['alex', 'vova', 'tolya'];
$arr1 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
$arr2 =  ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
$arr3 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];
?>
<table>
    <tr bgcolor="#2E8B57">
        <td>Имя</td>
        <td>Возраст</td>
        <td>Число PI</td>
    </tr>
    <tr bgcolor="#A9A9A9">
        <td><?=$myName?></td>
        <td><?=$age?></td>
        <td><?=$pi?></td>
    </tr>
</table>
<br>
<pre>
    <?php print_r($arr);?>
</pre>
<pre>
    <?php print_r($arr1);?>
</pre>
<pre>
    <?php print_r($arr2);?>
</pre>
<pre>
    <?php print_r($arr3);?>
</pre>
