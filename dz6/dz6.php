<?php

##1*-----------------------------------------------------------
echo '<h3>#1*</h3>';

function getProductArray()
{
    $arr = [];
    $arrProductEvenIndex = 1;
    $arrOddIndex = [];
    for ($i = 0; $i <= 100; $i++) {
        $arr[] = rand(1, 100);
    }
    for ($i = 0; $i <= 100; $i++) {
        if ($i % 2 == 0) {
            $arrProductEvenIndex *= $arr[$i];
        } else {
            $arrOddIndex[] = $arr[$i];
        }
    }
    echo '<pre> Произведение четных индексов: ' . $arrProductEvenIndex . '<br>';
    print_r($arrOddIndex);
    echo '</pre>';
    return $arrProductEvenIndex . $arrOddIndex;
}

getProductArray();

##2*-----------------------------------------------------------
echo '<br><br>' . '<h3>#2*</h3>';

function getSum($number1, $number2): int
{
    $sum = $number1 + $number2;
    return $sum;
}

function getProduct($number1, $number2): int
{
    $product = $number1 * $number2;
    return $product;
}

echo 'Произведение чисел: ' . getProduct(5, 6) . '<br>' .
    'Сумма чисел: ' . getSum(5, 6);

##3*-----------------------------------------------------------
echo '<br><br>' . '<h3>#3*</h3>';

function getAverage($number1, $number2, $number3): float
{
    $average = ($number1 + $number2 + $number3) / 3;
    return $average;
}

echo 'Среднее арифметическое чисел: ' . getAverage(11, 5, 9);

##4*-----------------------------------------------------------
echo '<br><br>' . '<h3>#4*</h3>';

function getIncreaseNumber30Percent($number1): int
{
    $IncreaseNumber30 = $number1 * 1.3;
    return $IncreaseNumber30;
}

function getIncreaseNumber120Percent($number1): int
{
    $IncreaseNumber120 = $number1 * 2.2;
    return $IncreaseNumber120;
}

echo 'Увеличенное число на 30%: ' . getIncreaseNumber30Percent(5) . '<br>' .
    'Увеличенное число на 120%: ' . getIncreaseNumber120Percent(5);

##5*-----------------------------------------------------------
echo '<br><br>' . '<h3>#5*</h3>';
?>
    <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
        <label>Выбирете страну для отдыха: </label>
        <br>
        <select name="country">
            <option value="Turkey">Турция</option>
            <option value="Egypt">Египет</option>
            <option value="Italy">Италия</option>
        </select>
        <br><br>
        <label>Выбирете количество дней для отдыха: </label>
        <br>
        <input type="number" name="numberOfDays" min="0">
        <br><br>
        <label>Наличие скидки: </label>
        <br>
        <select name="discount">
            <option value="no">Нет скидки</option>
            <option value="yes">Есть скидка</option>
        </select>
        <br><br>
        <input type="submit" value="Посчитать стоимость" name="go">
    </form>
<?php
function getPriceForRest(): int
{
    $priceForDay = 400 * $_POST['numberOfDays'];
    if ($_POST['country'] == 'Egypt') {
        $priceForDay *= 1.1;
    } elseif ($_POST['country'] == 'Italy') {
        $priceForDay *= 1.12;
    }
    if ($_POST['discount'] == 'yes') {
        $priceForDay /= 1.05;
    }
    return $priceForDay;
}

if ($_POST['country']) {
    echo '<hr>Итоговая стоимость за отдых составляет: ' . getPriceForRest() . ' гривен';
}

##6*-----------------------------------------------------------
echo '<br><br>' . '<h3>#6*</h3>';
?>
    <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
        <label>Ваше имя:</label>
        <br>
        <input name="name" type="text" value="<?= $_POST['name'] ?>" placeholder="Введите ваше имя">
        <br><br>
        <label>Ваша почта:</label>
        <br>
        <input name="email" type="email" value="<?= $_POST['email'] ?>" placeholder="Введите вашу почту">
        <br><br>
        <label>Ваш пароль:</label>
        <br>
        <input name="password" type="password" placeholder="Введите ваш пароль">
        <br><br>
        <input type="submit" value="Регистрация" name="go">
    </form>
<?php

function registration($name, $email, $password): string
{
    if (empty($name)) {
        $message = 'Введите ваше имя!';
    } elseif (empty($email)) {
        $message = 'Введите вашу почту!';
    } elseif (empty($password)) {
        $message = 'Введите ваш пароль!';
    }
    return $message;
}

if ($_POST['name'] || $_POST['email'] || $_POST['password']) {
    echo '<br>' . registration($_POST['name'], $_POST['email'], $_POST['password']);
}

##7*-----------------------------------------------------------
echo '<br><br>' . '<h3>#7*</h3>';
?>
    <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
        <label>Введите число:</label>
        <br>
        <input name="number" type="number">
        <br><br>
        <input type="submit" value="Вывод" name="go">
    </form>
<?php
function getTextMassege($number)
{
    if ($number > 0) {
        for ($i = 0; $i < $number; $i++) {
            echo 'Silence is golden<br>';
        }
    } else {
        echo 'Silence is golden<br>';
    }
}

if ($_POST['number']) {
    echo getTextMassege($_POST['number']);
}

##8*-----------------------------------------------------------
echo '<br><br>' . '<h3>#8*</h3>';
?>
    <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
        <label>Введите число:</label>
        <br>
        <input name="number" type="number">
        <br><br>
        <input type="submit" value="Вывод" name="go">
    </form>
<?php
function getArrNullOne($number): array
{
    $arr = [];
    for ($i = 0; $i < $number; $i++) {
        if ($i % 2 == 0) {
            $arr[] = 0;
        } else {
            $arr[] = 1;
        }
    }
    return $arr;
}

print_r(getArrNullOne($_POST['number']));

##9*-----------------------------------------------------------
echo '<br><br>' . '<h3>#9*</h3>';

function getArrDuplicateValues()
{
    $arr = ['Alex', 'Vlad', 'Alex', 'Igor', 'Igor', 'Kirill', 'Igor', 'Bogdan'];
    $arrDubl = [];
    foreach ($arr as $key => $value) {
        if (isset($arrDubl["$value"])) {
            $arrDubl["$value"] += 1;
        } else {
            $arrDubl["$value"] = 1;
        }
    }
    return $arrDubl;
}

print_r(getArrDuplicateValues());

##10*-----------------------------------------------------------
echo '<br><br>' . '<h3>#10*</h3>';

function getMinNum($num1, $num2, $num3): int
{
    $MinNum = $num1;
    if ($MinNum > $num2) {
        $MinNum = $num2;
    }
    if ($MinNum > $num3) {
        $MinNum = $num3;
    }
    return $MinNum;
}

function getMaxNum($num1, $num2, $num3): int
{
    $MaxNum = $num1;
    if ($MaxNum < $num2) {
        $MaxNum = $num2;
    }
    if ($MaxNum < $num3) {
        $MaxNum = $num3;
    }
    return $MaxNum;
}

echo getMinNum(2, 5, 8) . '<br>' . getMaxNum(2, 5, 8);

##11*-----------------------------------------------------------
echo '<br><br>' . '<h3>#11*</h3>';

function getSquareRectangle($height, $width)
{
    $square = $height * $width;
    return $square;
}

echo 'Площадь прямоугольника равна: ' . getSquareRectangle(4, 7);

##12*-----------------------------------------------------------
echo '<br><br>' . '<h3>#12*</h3>';

function pythagoreanTheorem($k1, $k2, $g)
{
    ($k1) ? $k1 = pow($k1, 2) : $find = 'k1';
    ($k2) ? $k2 = pow($k2, 2) : $find = 'k2';
    ($g) ? $g = pow($g, 2) : $find = 'g';

    switch ($find) {
        case 'k1':
            return sqrt($g - $k2);
            break;
        case 'k2':
            return sqrt($g - $k1);
            break;
        case 'g':
            return sqrt($k1 + $k2);
            break;
    }
    return false;
}

echo pythagoreanTheorem(5, false, 13);

##13*-----------------------------------------------------------
echo '<br><br>' . '<h3>#13*</h3>';

function perimeter($a, $b, $c, $d): int
{
    return $a + $b + $c + $d;
}

echo perimeter(5, 4, 5, 4);

##14*-----------------------------------------------------------
echo '<br><br>' . '<h3>#14*</h3>';

function discriminant($a, $b, $c)
{
    $dis = $b ** 2 - 4 * $a * $c;
    $x1 = (-$b + sqrt($dis)) / 2 * $a;
    $x2 = (-$b - sqrt($dis)) / 2 * $a;
    return 'x1 = ' . $x1 . '<br>' . 'x2 = ' . $x2;
}

echo discriminant(1, -2, -24);

##15-16*-----------------------------------------------------------
echo '<br><br>' . '<h3>#15-16*</h3>';

function evenNumbers()
{
    for ($i = 0; $i <= 100; $i++) {
        if ($i % 2 == 0) {
            $arrEvenNumbers[] = $i;
        }
    }
    return $arrEvenNumbers;
}

function oddNumbers()
{
    for ($i = 0; $i <= 100; $i++) {
        if (!($i % 2 == 0)) {
            $arrOddNumbers[] = $i;
        }
    }
    return $arrOddNumbers;
}

echo "<pre>";
print_r(evenNumbers());
print_r(oddNumbers());
echo "</pre>";

##1**-----------------------------------------------------------
echo '<br><br>' . '<h3>#1**</h3>';

function exponentiation($number, $exponent)
{
    return $number ** $exponent;
}

echo exponentiation(3, 3);

##2**-----------------------------------------------------------
echo '<br><br>' . '<h3>#2**</h3>';

$arr = [];
for ($i = 0; $i < 10; $i++) {
    $arr[] = rand(1, 10);
}

function sortArr($arr, $a = Null)
{
    $countArr = 0;
    for ($i = 0; true; $i++) {
        if ($arr[$i] != null) {
            $countArr++;
        } else {
            break;
        }
    }
    if (!$a) {
        for ($i = 0; $i < $countArr - 1; $i++) {
            if ($arr[$i] > $arr[$i + 1]) {
                $numbMore = $arr[$i];
                $arr[$i] = $arr[$i + 1];
                $arr[$i + 1] = $numbMore;
                $i = -1;
            }
        }
    } else {
        for ($i = 0; $i < $countArr - 1; $i++) {
            if ($arr[$i] < $arr[$i + 1]) {
                $numbMore = $arr[$i];
                $arr[$i] = $arr[$i + 1];
                $arr[$i + 1] = $numbMore;
                $i = -1;
            }
        }
    }
    return $arr;
}

echo "<pre>";
print_r(sortArr($arr));
echo "</pre>";

##3**-----------------------------------------------------------
echo '<br><br>' . '<h3>#3**</h3>';

$arr = ['Alex', 'Vlad', 'Alex', 'Igor', 'Igor', 'Kirill', 'Igor', 'Bogdan'];
function searchArr($arr, $find)
{
    $countArr = count($arr) - 1;
    for ($i = 0; $i < $countArr; $i++) {
        if ($arr[$i] == $find) {
            $numbOfFinds++;
        }
    }
    return $numbOfFinds;
}
echo 'Количество находок: '.searchArr($arr, 'Vlad');
