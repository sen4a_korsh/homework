<?php

##1*-----------------------------------------------------------
echo '<h3>#1*</h3>';

$minNumFn = function ($num1, $num2, $num3) {
    if ($num1 < $num2 && $num1 < $num3) {
        $minNum = $num1;
    } elseif ($num2 < $num1 && $num2 < $num3) {
        $minNum = $num2;
    } elseif ($num3 < $num1 && $num3 < $num2) {
        $minNum = $num3;
    }
    return $minNum;
};
echo 'Минимальное число: ' . $minNumFn(2, 1, 8) . '<br>';

$maxNumFn = function ($num1, $num2, $num3) {
    if ($num1 > $num2 && $num1 > $num3) {
        $maxNum = $num1;
    } elseif ($num2 > $num1 && $num2 > $num3) {
        $maxNum = $num2;
    } elseif ($num3 > $num1 && $num3 > $num2) {
        $maxNum = $num3;
    }
    return $maxNum;
};
echo 'Максимальное число: ' . $maxNumFn(2, 1, 8);


##2*-----------------------------------------------------------
echo '<br><br>' . '<h3>#2*</h3>';

$Square = function ($a, $b) {
    $s = $a * $b;
    return $s;
};

echo 'Площадь равна: ' . $Square(5, 8) . '<br>';

$square2 = fn($a, $b) => $a * $b;
echo 'Площадь равна: ' . $square2(5, 8);

##3*-----------------------------------------------------------
echo '<br><br>' . '<h3>#3*</h3>';

$pifag = function ($k1, $k2, $g) {
    ($k1) ? $k1 = pow($k1, 2) : $find = 'k1';
    ($k2) ? $k2 = pow($k2, 2) : $find = 'k2';
    ($g) ? $g = pow($g, 2) : $find = 'g';
    switch ($find) {
        case 'k1':
            return sqrt($g - $k2);
            break;
        case 'k2':
            return sqrt($g - $k1);
            break;
        case 'g':
            return sqrt($k1 + $k2);
            break;
    }
    return false;
};
echo $pifag(5, false, 13);

##4*-----------------------------------------------------------
echo '<br><br>' . '<h3>#4*</h3>';

$perimeter = function ($a, $b, $c, $d) {
    return $a + $b + $c + $d;
};
echo 'Периметр: ' . $perimeter(5, 4, 5, 4) . '<br>';

$perimeter1 = fn($a, $b, $c, $d) => $a + $b + $c + $d;
echo 'Периметр: ' . $perimeter1(5, 4, 5, 4);

##5*-----------------------------------------------------------
echo '<br><br>' . '<h3>#5*</h3>';

$discriminant = function ($a, $b, $c) {
    $dis = $b ** 2 - 4 * $a * $c;
    return $dis;
};
echo 'Дискриминант: ' . $discriminant(1, -2, -24) . '<br>';

$discriminant1 = fn($a, $b, $c) => $b ** 2 - 4 * $a * $c;
echo 'Дискриминант: ' . $discriminant1(1, -2, -24) . '<br>';

##6-7*-----------------------------------------------------------
echo '<br><br>' . '<h3>#6-7*</h3>';

$evenNumbers = function () {
    for ($i = 0; $i <= 100; $i++) {
        if ($i % 2 == 0) {
            $arrEvenNumbers[] = $i;
        }
    }
    return $arrEvenNumbers;
};
$oddNumbers = function () {
    for ($i = 0; $i <= 100; $i++) {
        if (!($i % 2 == 0)) {
            $arrOddNumbers[] = $i;
        }
    }
    return $arrOddNumbers;
};

echo "<pre>";
print_r($evenNumbers());
print_r($oddNumbers());
echo "</pre>";

##8*-----------------------------------------------------------
echo '<br><br>' . '<h3>#8*</h3>';

$arr = ['Alex', 'Vlad', 'Alex', 'Igor', 'Igor', 'Kirill', 'Igor', 'Bogdan'];
$ArrDuplicateValues = function ($arr) {
    $arrDubl = [];
    foreach ($arr as $key => $value) {
        if (isset($arrDubl["$value"])) {
            $arrDubl["$value"] += 1;
        } else {
            $arrDubl["$value"] = 1;
        }
    }
    return $arrDubl;
};

print_r($ArrDuplicateValues($arr));
