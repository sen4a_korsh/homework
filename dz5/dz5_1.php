<?php

##1**-----------------------------------------------------------
echo '<h3>#1**</h3>';
for ($i = 2; $i <= 100; $i++) {
    for ($j = 2; $j < $i; $j++) {
        if ($i % $j == 0) {
            continue(2);
        }
    }
    echo $i . '<br>';
}

##2**-----------------------------------------------------------
echo '<br><br>' . '<h3>#2**</h3>';
$count = 0;
for ($i = 0; $i <= 100; $i++) {
    $x = rand(0, 100);
//    echo $x. '<br>';
    if ($x % 2 == 0) {
        $count++;
    }
}
echo 'Количество четных чисел: ' . $count;

##3**-----------------------------------------------------------
echo '<br><br>' . '<h3>#3**</h3>';
$count1 = 0;
$count2 = 0;
$count3 = 0;
$count4 = 0;
$count5 = 0;

for ($i = 0; $i <= 100; $i++) {
    $x = rand(1, 5);
//    echo $x. '<br>';
    switch ($x) {
        case 1:
            $count1++;
            break;
        case 2:
            $count2++;
            break;
        case 3:
            $count3++;
            break;
        case 4:
            $count4++;
            break;
        case 5:
            $count5++;
            break;
    }
}
echo 'Количество едениц: ' . $count1 . '<br>
      Количество двоек: ' . $count2 . '<br>
      Количество троек: ' . $count3 . '<br>
      Количество четверок: ' . $count4 . '<br>
      Количество пятерок: ' . $count5 . '<br>';

##4**-----------------------------------------------------------
echo '<br><br>' . '<h3>#4**</h3>';

$color = ['red', 'blue', 'green', 'purple', 'yellow'];

echo '<table border="1" cellpadding="5">';
for ($y = 1; $y <= 3; $y++) {

    echo '<tr>';
    for ($x = 1; $x <= 5; $x++) {

        echo '<td bgcolor="' . $color[rand(0, 4)] . '">' . 'Цвет' . '</td>';
    }
    echo '</tr>';
}
echo '</table>';

##5**-----------------------------------------------------------
echo '<br><br>' . '<h3>#5**</h3>';
$month = rand(1, 12);
switch ($month) {
    case $month <= 2 || $month == 12:
        echo "На улице холодно, блин - зима";
        break;
    case $month <= 5 :
        echo "На улице все тает, блин - весна";
        break;
    case $month <= 8 :
        echo "На улице жарко, блин - лето";
        break;
    case $month <= 11 :
        echo "На улице мокро, блин - осень";
        break;
}

##6**-----------------------------------------------------------
echo '<br><br>' . '<h3>#6**</h3>';

$str = 'abcde';

if ($str[0] == 'a') {
    echo 'oh, yes';
} else {
    echo 'oh, no';
}

##7**-----------------------------------------------------------
echo '<br><br>' . '<h3>#7**</h3>';

$str = '12345';
if ($str[0] == '1' || $str[0] == '2' || $str[0] == '3') {
    echo 'oh, yes';
} else {
    echo 'oh, no';
}

##8**-----------------------------------------------------------
echo '<br><br>' . '<h3>#8**</h3>';
$test = true;
if ($test == true) {
    echo 'Verno';

} else {
    echo 'Ne verno';
}
echo ($test == true) ? 'Verno' : 'Ne verno';

##9**-----------------------------------------------------------
echo '<br><br>' . '<h3>#9**</h3>';
$daysRu = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$daysEn = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
$lang = 'ru';
if ($lang == 'ru') {
    echo 'Дни недели: <br>';
    for ($i = 0; $i <= count($daysRu); $i++) {
        echo $daysRu[$i] . '<br>';
    }
} elseif ($lang == 'en') {
    echo 'Days of the week: <br>';
    for ($i = 0; $i <= count($daysEn); $i++) {
        echo $daysEn[$i] . '<br>';
    }
}

##10**-----------------------------------------------------------
echo '<br><br>' . '<h3>#10**</h3>';

$clock = rand(0, 59);
echo $clock . '<br>';

if ($clock <= 15) {
    echo 'Число находится в первой четверти';
} elseif ($clock <= 30) {
    echo 'Число находится во второй четверти';
} elseif ($clock <= 45) {
    echo 'Число находится в третей четверти';
} else {
    echo 'Число находится в четвертой четверти';
}
echo '<br>';

echo $clock <= 15 ? 'Число находится в первой четверти' : '';
echo $clock > 15 && $clock <= 30 ? 'Число находится во второй четверти' : '';
echo $clock > 30 && $clock <= 45 ? 'Число находится в третей четверти' : '';
echo $clock > 45 && $clock <= 59 ? 'Число находится в четвертой четверти' : '';

##1***-----------------------------------------------------------
echo '<br><br>' . '<h3>#1***</h3>';

$daysRu = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$count = 0;
for ($i=0; true; $i++) {
    if ($daysRu[$i] != null) {
        $count++;
    } else {
        break;
    }
}
echo $count . '<br>';

$count = 0;
$i = 0;
while (true) {
    if ($daysRu[$i] != null) {
        $count++;
    } else {
        break;
    }
    $i++;
}
echo $count . '<br>';

$count = 0;
$i = 0;
do {
    if ($daysRu[$i] != null) {
        $count++;
    } else {
        break;
    }
    $i++;
} while (true);
echo $count . '<br>';

$count = 0;
foreach ($daysRu as $value) {
    $count++;
}
echo $count;

##2***-----------------------------------------------------------
echo '<br><br>' . '<h3>#2***</h3>';

$name = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$nameR = [];
$count = count($name) - 1;
foreach ($name as $value) {
    $nameR[$count] = $value;
    $count--;
}
print_r($name);
echo '<br>';
print_r($nameR);
echo '<br>';

$name = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$nameR = [];
$count = count($name) - 1;
$numArr = count($name) - 1;
for ($i = 0; $i <= $numArr; $i++) {
    $nameR[$i] = $name[$count];
    $count--;
}
print_r($name);
echo '<br>';
print_r($nameR);
echo '<br>';

$name = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$nameR = [];
$count = count($name) - 1;
$numArr = count($name) - 1;
$i = 0;
while ($i <= $numArr) {

    $nameR[$i] = $name[$count];
    $i++;
    $count--;
}
print_r($name);
echo '<br>';
print_r($nameR);
echo '<br>';

$name = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$nameR = [];
$count = count($name) - 1;
$numArr = count($name) - 1;
$i = 0;
do {
    $nameR[$i] = $name[$count];
    $i++;
    $count--;
} while ($i <= $numArr);
print_r($name);
echo '<br>';
print_r($nameR);
echo '<br>';

##3***-----------------------------------------------------------
echo '<br><br>' . '<h3>#3***</h3>';

$name = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$nameR = [];
$count = count($name) - 1;
foreach ($name as $value) {
    $nameR[$count] = $value;
    $count--;
}
print_r($name);
echo '<br>';
print_r($nameR);
echo '<br>';

$num = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$numR = [];
$count = count($num) - 1;
$numArr = count($num) - 1;
for ($i = 0; $i <= $numArr; $i++) {
    $numR[$i] = $num[$count];
    $count--;
}
print_r($num);
echo '<br>';
print_r($numR);
echo '<br>';

$num = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$numR = [];
$count = count($num) - 1;
$numArr = count($num) - 1;
$i = 0;
while ($i <= $numArr) {
    $numR[$i] = $num[$count];
    $count--;
    $i++;
}
print_r($num);
echo '<br>';
print_r($numR);
echo '<br>';

$num = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$numR = [];
$count = count($num) - 1;
$numArr = count($num) - 1;
$i = 0;
do {
    $numR[$i] = $num[$count];
    $count--;
    $i++;
} while ($i <= $numArr);
print_r($num);
echo '<br>';
print_r($numR);
echo '<br>';

##4***-----------------------------------------------------------
echo '<br><br>' . '<h3>#4***</h3>';

$str = 'Hi I am ALex';
$strArr = str_split($str, 1);
$strR = '';
$count = strlen($str) - 1;
foreach ($strArr as $value) {
    $strR .= $strArr[$count];
    $count--;
}
echo $str . '<br>' . $strR . '<br><br>';


$str = 'Hi I am ALex';
$strR = '';
$count = strlen($str) - 1;
$numStr = strlen($str) - 1;
for ($i = 0; $i <= $numStr; $i++) {
    $strR .= $str[$count];
    $count--;
}
echo $str . '<br>' . $strR . '<br><br>';

$str = 'Hi I am ALex';
$strR = '';
$count = strlen($str) - 1;
$numStr = strlen($str) - 1;
$i = 0;
while ($i <= $numStr) {
    $i++;
    $strR .= $str[$count];
    $count--;
}
echo $str . '<br>' . $strR . '<br><br>';

$str = 'Hi I am ALex';
$strR = '';
$count = strlen($str) - 1;
$numStr = strlen($str) - 1;
$i = 0;
do {
    $i++;
    $strR .= $str[$count];
    $count--;

} while ($i <= $numStr);
echo $str . '<br>' . $strR . '<br><br>';

##5***-----------------------------------------------------------
echo '<br><br>' . '<h3>#5***</h3>';

$str = 'Hi I am ALex';
echo $str . '<br>';
$strSmall = mb_strtolower($str);
$strArrSm = str_split($strSmall, 1);
foreach ($strArrSm as $value) {
    echo $value;
}
echo '<br>';

$str = 'Hi I am ALex';
echo $str . '<br>';
$strSmall = mb_strtolower($str);
$numStr = strlen($strSmall) - 1;
for ($i = 0; $i <= $numStr; $i++) {
    echo $strSmall[$i];
}
echo '<br>';

$str = 'Hi I am ALex';
echo $str . '<br>';
$strSmall = mb_strtolower($str);
$numStr = strlen($strSmall) - 1;
$i = 0;
while ($i <= $numStr) {
    echo $strSmall[$i];
    $i++;
}
echo '<br>';

$str = 'Hi I am ALex';
echo $str . '<br>';
$strSmall = mb_strtolower($str);
$numStr = strlen($strSmall) - 1;
$i = 0;
do {
    echo $strSmall[$i];
    $i++;
} while ($i <= $numStr);
echo '<br>';

##6***-----------------------------------------------------------
echo '<br><br>' . '<h3>#6***</h3>';

$str = 'Hi I am ALex';
echo $str . '<br>';
$strBig = strtoupper($str);
$strArrSm = str_split($strBig, 1);
foreach ($strArrSm as $value) {
    echo $value;
}
echo '<br>';

$str = 'Hi I am ALex';
echo $str . '<br>';
$strBig = strtoupper($str);
$numStr = strlen($strBig) - 1;
for ($i = 0; $i <= $numStr; $i++) {
    echo $strBig[$i];
}
echo '<br>';

$str = 'Hi I am ALex';
echo $str . '<br>';
$strBig = strtoupper($str);
$numStr = strlen($strBig) - 1;
$i = 0;
while ($i <= $numStr) {
    echo $strBig[$i];
    $i++;
}
echo '<br>';

$str = 'Hi I am ALex';
echo $str . '<br>';
$strBig = strtoupper($str);
$numStr = strlen($strBig) - 1;
$i = 0;
do {
    echo $strBig[$i];
    $i++;
} while ($i <= $numStr);
echo '<br>';

##7***-----------------------------------------------------------
echo '<br><br>' . '<h3>#7***</h3>';

$str = 'Hi I am ALex';
$strArr = str_split($str, 1);
$strR = '';
$count = strlen($str) - 1;
foreach ($strArr as $value) {
    $strR .= $strArr[$count];
    $count--;
}
echo $str . '<br>' . $strR . '<br><br>';


$str = 'Hi I am ALex';
$strR = '';
$count = strlen($str) - 1;
$numStr = strlen($str) - 1;
for ($i = 0; $i <= $numStr; $i++) {
    $strR .= $str[$count];
    $count--;
}
echo $str . '<br>' . $strR . '<br><br>';

$str = 'Hi I am ALex';
$strR = '';
$count = strlen($str) - 1;
$numStr = strlen($str) - 1;
$i = 0;
while ($i <= $numStr) {
    $i++;
    $strR .= $str[$count];
    $count--;
}
echo $str . '<br>' . $strR . '<br><br>';

$str = 'Hi I am ALex';
$strR = '';
$count = strlen($str) - 1;
$numStr = strlen($str) - 1;
$i = 0;
do {
    $i++;
    $strR .= $str[$count];
    $count--;

} while ($i <= $numStr);
echo $str . '<br>' . $strR . '<br><br>';

##8***-----------------------------------------------------------
echo '<br><br>' . '<h3>#8***</h3>';

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSm = [];
$i = 0;
foreach ($arr as $value) {
    $arrSm[$i] = strtoupper($value);
    $i++;
}
print_r($arr);
echo '<br>';
print_r($arrSm);
echo '<br>';


$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSm = [];
$count = count($arr) - 1;
for ($i = 0; $i <= $count; $i++) {
    $arrSm[$i] = strtoupper($arr[$i]);
}
print_r($arr);
echo '<br>';
print_r($arrSm);
echo '<br>';

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSm = [];
$count = count($arr) - 1;
$i = 0;
while ($i <= $count) {
    $arrSm[$i] = strtoupper($arr[$i]);
    $i++;
}
print_r($arr);
echo '<br>';
print_r($arrSm);
echo '<br>';

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSm = [];
$count = count($arr) - 1;
$i = 0;
do {
    $arrSm[$i] = strtoupper($arr[$i]);
    $i++;
} while ($i <= $count);
print_r($arr);
echo '<br>';
print_r($arrSm);
echo '<br>';

##9***-----------------------------------------------------------
echo '<br><br>' . '<h3>#9***</h3>';

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSm = [];
$i = 0;
foreach ($arr as $value) {
    $arrSm[$i] = strtoupper($value);
    $i++;
}
print_r($arr);
echo '<br>';
print_r($arrSm);
echo '<br>';


$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSm = [];
$count = count($arr) - 1;
for ($i = 0; $i <= $count; $i++) {
    $arrSm[$i] = strtoupper($arr[$i]);
}
print_r($arr);
echo '<br>';
print_r($arrSm);
echo '<br>';

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSm = [];
$count = count($arr) - 1;
$i = 0;
while ($i <= $count) {
    $arrSm[$i] = strtoupper($arr[$i]);
    $i++;
}
print_r($arr);
echo '<br>';
print_r($arrSm);
echo '<br>';

$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSm = [];
$count = count($arr) - 1;
$i = 0;
do {
    $arrSm[$i] = strtoupper($arr[$i]);
    $i++;
} while ($i <= $count);
print_r($arr);
echo '<br>';
print_r($arrSm);
echo '<br>';

##10***-----------------------------------------------------------
echo '<br><br>' . '<h3>#10***</h3>';

$num = 1234678;
$arrNum = array_map('intval', str_split($num));
$count = strlen($num) - 1;
$numR = '';
foreach ($arrNum as $value) {
    $numR .= $arrNum[$count];
    $count--;
}
echo $num . '<br>' . $numR . '<br><br>';

$num = 1234678;
$strNum = strval($num);
$count = strlen($strNum) - 1;
$numR = '';
for ($i = $count; $i >= 0; $i--) {
    $numR .= $strNum{$i};
}
echo $num . '<br>' . $numR . '<br><br>';

$num = 1234678;
$strNum = strval($num);
$count = strlen($strNum) - 1;
$numR = '';
$i = $count;
while ($i >= 0) {
    $numR .= $strNum{$i};
    $i--;
}
echo $num . '<br>' . $numR . '<br><br>';

$num = 1234678;
$strNum = strval($num);
$count = strlen($strNum) - 1;
$numR = '';
$i = $count;
do {
    $numR .= $strNum{$i};
    $i--;
} while ($i >= 0);
echo $num . '<br>' . $numR . '<br><br>';

##11***-----------------------------------------------------------
echo '<br><br>' . '<h3>#11***</h3>';

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
print_r($arr);
$countArr = count($arr) - 1;
for ($i = 0; $i < $countArr; $i++) {
    if ($arr[$i] > $arr[$i + 1]) {
        $numbMore = $arr[$i];
        $arr[$i] = $arr[$i + 1];
        $arr[$i + 1] = $numbMore;
        $i = -1;
    }
}
echo '<br>';
print_r($arr);
echo '<br><br>';

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
print_r($arr);
$countArr = count($arr) - 1;
$i = 0;
while ($i < $countArr) {
    if ($arr[$i] > $arr[$i + 1]) {
        $numbMore = $arr[$i];
        $arr[$i] = $arr[$i + 1];
        $arr[$i + 1] = $numbMore;
        $i=-1;
    }
    $i++;
}
echo '<br>';
print_r($arr);
echo '<br><br>';

$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
print_r($arr);
$countArr = count($arr) - 1;
$i = 0;
do{
    if ($arr[$i] > $arr[$i + 1]) {
        $numbMore = $arr[$i];
        $arr[$i] = $arr[$i + 1];
        $arr[$i + 1] = $numbMore;
        $i=-1;
    }
    $i++;

}while($i < $countArr);
echo '<br>';
print_r($arr);
echo '<br><br>';

